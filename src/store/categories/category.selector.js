import {createSelector} from "reselect"; 


const selectCategoryReducer = (state) => {
    return state.categories;
}
export const selectCategories = createSelector(
    [selectCategoryReducer],
    (categoriesSlice) => {
        return categoriesSlice.categories;
    } 
);

export const selectCategoriesMap = createSelector(
    [selectCategories],
    (categories) => {
        return categories.reduce((accumu, category) => {
        const {title, items} = category;
        accumu[title.toLowerCase()] = items;
        return accumu;
        }, {});
    }
);

export const selectCategoryIsLoading = createSelector(
    [selectCategoryReducer],
    (categoriesSlice) => {
        return categoriesSlice.isLoading;
    }
)
