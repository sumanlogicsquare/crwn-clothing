import React, { Fragment } from "react";
import { useSelector } from "react-redux";
import {
    selectCategoriesMap,
    selectCategoryIsLoading,
} from "../../store/categories/category.selector";
import CategoryPreview from "../../components/categoryPreview/CategoryPreview.components";
import Spinner from "../../components/spinner/Spinner.component";

const CategoriesPreview = () => {
    const categoriesMap = useSelector(selectCategoriesMap);
    const isLoading = useSelector(selectCategoryIsLoading);

    return (
        <Fragment>
            {isLoading ? (
                <Spinner />
            ) : (
                Object.keys(categoriesMap).map((title) => {
                    const products = categoriesMap[title];

                    return (
                        <CategoryPreview
                            key={title}
                            title={title}
                            products={products}
                        />
                    );
                })
            )}
        </Fragment>
    );
};

export default CategoriesPreview;
