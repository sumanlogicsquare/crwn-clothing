import React, { Fragment, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import {
    selectCategoriesMap,
    selectCategoryIsLoading,
} from "../../store/categories/category.selector";
import ProductCard from "../../components/productCard/ProductCard.component";
import Spinner from "../../components/spinner/Spinner.component";

import "./category.styles.scss";

const Category = () => {
    const { category } = useParams();
    const categoriesMap = useSelector(selectCategoriesMap);
    const isLoading = useSelector(selectCategoryIsLoading);
    const [products, setProducts] = useState(categoriesMap[category]);

    useEffect(() => {
        setProducts(categoriesMap[category]);
    }, [category, categoriesMap]);

    return (
        <Fragment>
            <h2 className="categoryTitle">{category.toUpperCase()}</h2>
            {isLoading ? (
                <Spinner />
            ) : (
                <div className="categoryContainer">
                    {products &&
                        products.map((product) => {
                            return (
                                <ProductCard
                                    key={product.id}
                                    product={product}
                                />
                            );
                        })}
                </div>
            )}
        </Fragment>
    );
};

export default Category;
