import React from "react";
import { useSelector } from "react-redux";
import {
    selectCartItems,
    selectCartTotal,
} from "../../store/cart/cart.selector";

import CheckoutItem from "../../components/checkoutItem/CheckoutItem.component";

import "./checkout.styles.scss";

const Checkout = () => {
    const cartItems = useSelector(selectCartItems);
    const cartTotal = useSelector(selectCartTotal);

    return (
        <div className="checkoutContainer">
            <div className="checkoutHeader">
                <div className="headerBlock">
                    <span>Product</span>
                </div>
                <div className="headerBlock">
                    <span>Description</span>
                </div>
                <div className="headerBlock">
                    <span>Quantity</span>
                </div>
                <div className="headerBlock">
                    <span>Price</span>
                </div>
                <div className="headerBlock">
                    <span>Remove</span>
                </div>
            </div>
            {cartItems.map((cartItem) => {
                return <CheckoutItem key={cartItem.id} cartItem={cartItem} />;
            })}
            <span className="total">Total : ${cartTotal}</span>
        </div>
    );
};

export default Checkout;
