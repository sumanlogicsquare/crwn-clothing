import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { selectCartItems } from "../../store/cart/cart.selector";
import {
    addItemToCart,
    removeItemFromCart,
    clearItemFromCart,
} from "../../store/cart/cart.action";

import "./checkoutItem.styles.scss";

const CheckoutItem = ({ cartItem }) => {
    const { name, imageUrl, price, quantity } = cartItem;
    const dispatch = useDispatch();
    const cartItems = useSelector(selectCartItems);

    const clearItemHandler = () =>
        dispatch(clearItemFromCart(cartItems, cartItem));
    const addItemHandler = () => dispatch(addItemToCart(cartItems, cartItem));
    const removeItemHandler = () =>
        dispatch(removeItemFromCart(cartItems, cartItem));

    return (
        <div className="checkoutItemContainer">
            <div className="imageContainer">
                <img src={imageUrl} alt={`${name}`} />
            </div>
            <span className="name"> {name} </span>
            <span className="quantity">
                <div className="arrow" onClick={removeItemHandler}>
                    &#8722;
                </div>
                <span className="value">{quantity}</span>
                <div className="arrow" onClick={addItemHandler}>
                    &#43;
                </div>
            </span>
            <span className="price"> {price} </span>
            <div className="removeButton" onClick={clearItemHandler}>
                &#10008;
            </div>
        </div>
    );
};

export default CheckoutItem;

// <div key={id}>
//                     <h2>{name}</h2>
//                     <span>{quantity}</span>
//                     <spam onClick={() => addItemToCart(cartItem)}>Decrement</spam>
//                     <span onClick={() => removeItemFromCart(cartItem)}>Increment</span>
//                 </div>
