import React from "react";
import { useNavigate } from "react-router-dom";
import {
    BackgroundImage,
    Body,
    DirectoryItemContainer,
} from "./directoryItem.styles";
// import "./directoryItem.styles";

const DirectoryItem = (props) => {
    const { imageUrl, title, route } = props.category;

    const navigate = useNavigate();

    const navigationHandler = () => navigate(route);

    return (
        <DirectoryItemContainer onClick={navigationHandler}>
            <BackgroundImage imageUrl={imageUrl} />
            <Body>
                <h2>{title}</h2>
                <p>Shop Now</p>
            </Body>
        </DirectoryItemContainer>
    );
};

export default DirectoryItem;
