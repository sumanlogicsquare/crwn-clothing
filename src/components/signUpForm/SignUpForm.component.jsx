import React, { useState } from "react";
import {
    createAuthUserWithEmailAndPassword,
    createUserDocumentFromAuth,
} from "../../utils/firebase/firebase.utils";
import FormInput from "../formInput/FormInput.component";

import Button from "../button/Button.component";
import "./signUpForm.styles.scss";

const defaultFormFields = {
    displayName: "",
    email: "",
    password: "",
    confirmPassword: "",
};
export const SignUpForm = () => {
    const [formFields, setFormFields] = useState(defaultFormFields);
    const { displayName, email, password, confirmPassword } = formFields;

    // console.log(formFields);

    const resetFormFields = () => {
        setFormFields(defaultFormFields);
    };

    const onSubmitHandler = async (event) => {
        event.preventDefault();
        if (password !== confirmPassword) {
            alert("Password do not match");
            return;
        }
        try {
            const { user } = await createAuthUserWithEmailAndPassword(
                email,
                password
            );

            await createUserDocumentFromAuth(user, {
                displayName,
            });

            resetFormFields();
        } catch (error) {
            if (error.code === "auth/email-already-in-use") {
                alert("Email already in use");
            } else {
                console.error("Error on Creating a User: ", error);
            }
        }
    };

    const onChanngeHandler = (event) => {
        const { name, value } = event.target;
        setFormFields({ ...formFields, [name]: value });
    };

    return (
        <div className="signUpContainer">
            <h2>Don't have an Account?</h2>
            <span>Sign-Up with Email and Password</span>
            <form onSubmit={onSubmitHandler}>
                <FormInput
                    label="Display Name"
                    type="text"
                    onChange={onChanngeHandler}
                    name="displayName"
                    value={displayName}
                    required
                />
                <FormInput
                    label="Email"
                    type="email"
                    onChange={onChanngeHandler}
                    name="email"
                    value={email}
                    required
                />
                <FormInput
                    label="Password"
                    type="Password"
                    onChange={onChanngeHandler}
                    name="password"
                    value={password}
                    minLength="6"
                    required
                />
                <FormInput
                    label="Confirm Password"
                    type="Password"
                    onChange={onChanngeHandler}
                    name="confirmPassword"
                    value={confirmPassword}
                    minLength="6"
                    required
                />
                <Button type="submit">Sign Up</Button>
            </form>
        </div>
    );
};

export default SignUpForm;
