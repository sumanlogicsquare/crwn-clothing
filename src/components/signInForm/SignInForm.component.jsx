import React, { useState } from "react";
import {
    createUserDocumentFromAuth,
    sighInWithGooglePopup,
    signInAuthUserWithEmailAndPassword,
} from "../../utils/firebase/firebase.utils";
import FormInput from "../formInput/FormInput.component";

import Button, { BUTTON_TYPE_CLASSES } from "../button/Button.component";
import "./signInForm.styles.scss";

const defaultFormFields = {
    email: "",
    password: "",
};
export const SignInForm = () => {
    const [formFields, setFormFields] = useState(defaultFormFields);
    const { email, password } = formFields;

    // console.log(formFields);

    const resetFormFields = () => {
        setFormFields(defaultFormFields);
    };

    const signInWithGoogle = async () => {
        try {
            await sighInWithGooglePopup();
        } catch (error) {
            if (error.code === "auth/popup-closed-by-user")
                alert("Declined Sign with Google!");
            else console.log(error);
        }
    };

    const onSubmitHandler = async (event) => {
        event.preventDefault();

        try {
            const { user } = await signInAuthUserWithEmailAndPassword(
                email,
                password
            );

            resetFormFields();
        } catch (error) {
            switch (error.code) {
                case "auth/wrong-password":
                    alert("Enter correct password!");
                    break;

                case "auth/user-not-found":
                    alert("User not exist!");
                    break;

                default:
                    console.error(error);
                    break;
            }
        }
    };

    const onChanngeHandler = (event) => {
        const { name, value } = event.target;
        setFormFields({ ...formFields, [name]: value });
    };

    return (
        <div className="signUpContainer">
            <h2>Already have an Account?</h2>
            <span>Sign-In with Email and Password</span>
            <form onSubmit={onSubmitHandler}>
                <FormInput
                    label="Email"
                    type="email"
                    onChange={onChanngeHandler}
                    name="email"
                    value={email}
                    required
                />
                <FormInput
                    label="Password"
                    type="Password"
                    onChange={onChanngeHandler}
                    name="password"
                    value={password}
                    minLength="6"
                    required
                />

                <div className="buttonsContainer">
                    <Button type="submit">Sign In</Button>
                    <Button
                        buttonType={BUTTON_TYPE_CLASSES.google}
                        type="button"
                        onClick={signInWithGoogle}
                    >
                        Google Sign In
                    </Button>
                </div>
            </form>
        </div>
    );
};

export default SignInForm;
